--
-- Структура таблицы `algos_rented_log`
--

CREATE TABLE IF NOT EXISTS `algos_rented_log` (
`id` int(11) NOT NULL,
  `data` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Индексы таблицы `algos_rented_log`
--
ALTER TABLE `algos_rented_log`
 ADD PRIMARY KEY (`id`), ADD KEY `date` (`date`);
-- --------------------------------------------------------


--
-- Структура таблицы `charts`
--

CREATE TABLE IF NOT EXISTS `charts` (
  `date` datetime NOT NULL,
  `data` text NOT NULL,
  `bitcoinUSDPrice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Индексы таблицы `charts`
--
ALTER TABLE `charts`
 ADD PRIMARY KEY (`date`);
-- --------------------------------------------------------

--
-- Структура таблицы `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `name` varchar(20) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
--
-- Индексы таблицы `data`
--
ALTER TABLE `data`
 ADD PRIMARY KEY (`name`);
-- --------------------------------------------------------

--
-- Структура таблицы `online_log`
--

CREATE TABLE IF NOT EXISTS `online_log` (
  `name` varchar(100) NOT NULL,
  `log` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Индексы таблицы `online_log`
--
ALTER TABLE `online_log`
 ADD PRIMARY KEY (`name`);
-- --------------------------------------------------------

--
-- Структура таблицы `prices`
--

CREATE TABLE IF NOT EXISTS `prices` (
`id` int(10) NOT NULL,
  `price` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Индексы таблицы `prices`
--
ALTER TABLE `prices`
 ADD PRIMARY KEY (`id`);
-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `uuid` varchar(40) NOT NULL,
  `name` text NOT NULL,
  `def_name` text,
  `miner` text NOT NULL,
  `def_miner` text,
  `miner_parameters` text NOT NULL,
  `def_miner_parameters` text,
  `comment` text NOT NULL,
  `def_comment` text,
  `last_status_sent` varchar(7) DEFAULT NULL,
  `time` int(11) NOT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
 ADD PRIMARY KEY (`uuid`);

--
-- добавлена колонка `btc_wallet` в таблицу `tasks`
---
ALTER TABLE `tasks` ADD `btc_wallet` VARCHAR(255) NULL DEFAULT NULL AFTER `uuid`;