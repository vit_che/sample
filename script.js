



function getStat(uuid){

    var uuid = uuid;

    $.ajax({
        type: "GET",
        url: window.location.href+'getstat.php',
        data: {
            uuid:uuid
        },
        dataType: 'json',
        success: function(data){
            console.log(' Success!');

            $('.dl').remove(); // for delete old data
            $('.modal-header').prepend("<h3 class='dl '>"+data.name+"</h3>");
            $('#tabl').append('<table class="dl"><thead><tr id="tr_date"><th>Date</th></tr></thead>'+
                '<tbody><tr id="tr_val"><td>OffLine</td></tr></tbody></table>');
            data.day.forEach(function (el){
                $('#tr_date').append('<th>'+el+'</th>');
            });
            data.val.forEach(function (el){
                $('#tr_val').append('<td>'+el+'</td>');
            });
        },
        error: function(data){
            console.log(' Error!');
            console.log(data);
        }
    });

}
