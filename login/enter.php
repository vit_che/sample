<?php

session_start();

require_once __DIR__ . '/../../charts/config.php';

$hash_enter = hash('sha256', $env_login.$env_pass);

if(isset($_COOKIE) && $_COOKIE['hash'] == $hash_enter ) {

    header("Location: /../hub/");
    exit;
}


if(isset($_POST) && $_POST['login'] != '' &&  $_POST['password'] != '' ) {

    $data = $_POST;

    if ($data["login"] == $env_login && $data["password"] == $env_pass) {
        # cookie expires in 1 year
        setcookie("hash", $hash_enter, time() + (365 * 24 * 60 * 60), '/');
        header("Location: /../hub/");
        exit;

    } else {

        exit ("LOGIN or/and PASSWORD are/is  UNCORRECT!".
            "<br><button class=\"btn btn-primary\" onclick=\"history.back()\">
                <a class=\"nav-link\" href=\"/libs/login\">Login</a>
                </button>");
    }

} else {

    exit ("LOGIN or/and PASSWORD are/is  EMPTY!".
        "<br><button class=\"btn btn-primary\" onclick=\"history.back()\">
                <a class=\"nav-link\" href=\"/libs/login\">Login</a>
                </button>");
}

?>
