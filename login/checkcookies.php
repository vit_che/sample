<?php

session_start();

require_once __DIR__ . '/../../charts/config.php';

$hash_enter = hash('sha256', $env_login.$env_pass);

if(isset($_COOKIE) && $_COOKIE['hash'] == $hash_enter ) {

    return;

} else {

    header("Location: /libs/login/");
    exit;
}

?>
