<?php

require_once __DIR__ . '/../mrr/mrr.php';

#gets minimum price for selected algo from mrr, exclude my rigs from calculation
function getAlgoMinPrice($algoName, $myRigIdArray) {
	global $mrr;
	
	$search = array(
	"type"=>$algoName, //algo
	"orderby"=>"price");
	$result = $mrr->get("/rig",$search);

	if ($result["success"] == "1") {
		$result = $result["data"]["records"];
	} else {
		exit("getAlgoMinPrice error getting $mrr->get(\"/rig\",$search)");
	}

	$minPricesArray = array();
	$minPrice = 9999;
	foreach ($result as $key => $rig) {
		# exclude our rigs from price calculation
		if (!(in_array($rig["id"], $myRigIdArray))) {
			if ($rig["price"]["BTC"]["price"]<$minPrice) {
				$minPrice = $rig["price"]["BTC"]["price"];
			}
			//echo $rig["id"]." ".$rig["price"]["BTC"]["price"]."<br>";
		} 
		
	}

	# if no rigs are currently being available for rental, return 0 minimum algo price
	if ($minPrice == 9999) {$minPrice = 0;}
	return $minPrice;
}

#returns UniqueAlgos and this hashrate
function getUniqueAlgos($rigList) {
	$algoArray = array();

	foreach ($rigList as $key => $rig) {
		if (!(in_array($rig["type"], $algoArray))) {
			$algoArray[] = $rig["type"];
		}

		$rigIdList[] = $rig["id"];
	}

	$minPricesArray = array();
	foreach ($algoArray as $key => $algo) {
		$minPricesArray[$algo] = getAlgoMinPrice($algo, $rigIdList);
	}
	
	return $minPricesArray;
}

$rigList = json_decode(file_get_contents("../mrr_data/my_rigs.txt"), true);
$algoMinPrices = getUniqueAlgos($rigList);

file_put_contents("unique_algo_prices.txt", json_encode($algoMinPrices));

?>