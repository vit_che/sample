<?php

require_once __DIR__ . '/../charts/base.php';
require_once __DIR__ . '/../mining_tasks/functions.php';


$uuid = $_GET['uuid'];

function getName($uuid){

    $query = "SELECT * FROM `tasks` WHERE `uuid` = '$uuid'";
    $result = base_query ($query);
    $result = $result->fetch_array(MYSQLI_ASSOC);
    return $result['name'];
}

function getDataTenDays($uuid){

    $name = getName($uuid); /* get name throw uuid*/
    $data = getAllLog()[$name]; /* get farm name data */

    $arr_day = array(); /*  get days array */
    for($i = 1; $i < 11; $i++){
        $arr_day[] = date('d/m', getStart() - 60*60*24*$i);
    }

    /*  get OFFTIME value  */
    $arr_val = array();
    foreach($data as $dat){
        foreach($arr_day as $day){
            if(date('d/m', $dat['start']) == $day) {
                $key = date('d/m', $dat['start']);
                $arr_val[$key] =  round($dat['offtime']*100/(60*60*24), 1);
            }
        }
    }

    $arr_day = array_reverse($arr_day);

    /* if some value absence */
    $arr = array();
    foreach($arr_day as $day){
        if(isset($arr_val[$day])){
            $key = $day;
            $arr += [ $key => $arr_val[$day]." %"];
        } else {
            $key = $day;
            $arr += [ $key => "no data"];
        }
    }

    $days = array_keys($arr);
    $vals = array_values($arr);

    $all = [
        'name' => $name,
        "day" => $days,
        "val" => $vals
    ];

    print_r(json_encode($all));
}

getDataTenDays($uuid);

?>
