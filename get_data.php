<?php
# returns table with last log records
# and buttons to add/remove monitored algos 

include_once 'functions.php';



# add new algo
if (isset($_POST["algo_to_add"])) {
	addAlgo($_POST["algo_to_add"]);
}

if (isset($_POST["delete_algo"])) {
	deleteAlgo($_POST["delete_algo"]);
}


# returns table with last log records
function getTable() {
	date_default_timezone_set ( "Europe/Kiev" );
	$date = date("Y-m-d H:i:s", time() - 60*60*24);
	
	$query = "SELECT * FROM `algos_rented_log` WHERE `date` > '$date' ORDER BY `date` DESC";
	$return = base_query ($query);
	
	$table = '
	<table id="AlgoTable" class="table table-striped table-bordered table-hover table-sm">
		<thead>
		<tr>
		<!--When a header is clicked, run the sortTable function, with a parameter,
		0 for sorting by names, 1 for sorting by country: -->
		<th onclick="sortTable(0)">date</th>
		<th onclick="sortTable(1)">algo</th>
		<th onclick="sortTable(2)">id</th>
		<th onclick="sortTable(3)">name</th>
		<th onclick="sortTable(4)">owner</th>
		<th onclick="sortTable(5)">price</th>
		<th onclick="sortTable(6)">advertised_hash</th>
		<th onclick="sortTable(7)">hashrate_30</th>
		<th onclick="sortTable(8)">rentedhours</th>		
		<th onclick="sortTable(9)">minhours</th>
		</tr>
		</thead>
	';

	$table_rows = '';
	while ($row = $return->fetch_array(MYSQLI_ASSOC)) {
		//echoPre($row);
		$rented_date = $row["date"];
		$rented_data = json_decode($row["data"], true);
		
		$algo = "<a href=\"https://www.miningrigrentals.com/rigs/".$rented_data["algo"]."\" target=\"_blank\">".$rented_data["algo"]."</a>";
		
		$id = "<a href=\"https://www.miningrigrentals.com/rigs/".$rented_data["id"]."\" target=\"_blank\">".$rented_data["id"]."</a>";
		
		$name = "<a href=\"https://www.miningrigrentals.com/rigs/".$rented_data["id"]."\" target=\"_blank\">".$rented_data["name"]."</a>";
		$owner = $rented_data["owner"];
		$price = $rented_data["price"];
		$advertised_hash = $rented_data["advertised_hash"];
		$hashrate_30 = $rented_data["hashrate_30"];
		$rentedhours = $rented_data["rentedhours"];
		$minhours = $rented_data["minhours"];

		$table_rows .= "<tr>
			<td>$rented_date</td>
			<td>$algo</td>
			<td>$id</td>
			<td>$name</td>
			<td>$owner</td>
			<td>$price</td>
			<td>$advertised_hash</td>
			<td>$hashrate_30</td>
			<td>$rentedhours</td>
			<td>$minhours</td>
			</tr>
			";
	}

	$table .= $table_rows;
	$table .= "</table>";
	
	echo $table;
}

#returns form to add/remove monitored algos
function getControlForm() {
	$algos_from_mrr_names = getAlgos();

	# get all algos to check from local file
	# returns "neoscrypt" => "46433,54228,54325,58108,61951,65674" ...
	$algos_and_rented_rigs_local = getAlgosAndRentedRigs();

	$add_algo_form = '
<form method="POST">
  <div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text" >Add algo:</span>
  </div>

    <select class="custom-select" name="algo_to_add">
      ';

      $options_list = '';
      foreach ($algos_from_mrr_names as $key => $algo_from_mrr_name) {
      	$options_list .= '<option value="'.$algo_from_mrr_name.'">'.$algo_from_mrr_name.'</option>';
      }
     
    $add_algo_form .= $options_list;

    $add_algo_form .= '</select>
    <div class="input-group-append">
      <button class="btn btn-success" style="width: 72px;" type="submit">Add</button>
    </div>
  </div>
</form>
	';

	$delete_algo_form = '';
	foreach ($algos_and_rented_rigs_local as $algo => $value) {
			$delete_algo_form .= '	
			<form method="POST">
				<div class="input-group">
				  <input type="text" disabled class="form-control" aria-label="..." value="'.$algo.'">
		
				  <div class="input-group-append">
				    <button class="btn btn-danger" type="submit" name="delete_algo" value="'.$algo.'">Delete</button>
		    	  </div>
				</div>	
			</form>
			';
	}

	$controlForm = $add_algo_form.$delete_algo_form;
	return $controlForm;
}


?>

