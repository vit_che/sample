<?php


if (isset($_POST["rigid"])) {
	setRigMinPrice($_POST["rigid"], $_POST["minprice"]);
	include_once 'cron.php';
}


function setRigMinPrice($rigId, $newPrice) {
	$rigsPrices = json_decode(file_get_contents("rig_min_prices.txt"),true);
	$rigsPrices[$rigId] = trim($newPrice);

	$fileData = json_encode($rigsPrices);
	file_put_contents("rig_min_prices.txt", $fileData);
}


?>