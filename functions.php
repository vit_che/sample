<?php

require_once __DIR__ . '/../charts/base.php';
require_once __DIR__ . '/../mrr/mrr.php';

function echoPre ($data) {
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}

# returns list of all avaialble algos from mrr
function getAlgos() {
	global $mrr;
	$result = $mrr->get("/info/algos");
	$result = $result["data"];
	$result_array = array();
	foreach ($result as $key => $value) {
		$result_array[] = $value["name"];
	}
	sort($result_array);
	return $result_array;
}

# returns list of rented rigs for selected algo
function getAlgoRented($algo) {
	global $mrr;

    $search = array(
        "count"=> 100,
        "offset"=> 0,
        "type"=>$algo, //algo
        "rented"=>"true"
    );

    $result = $mrr->get("/rig",$search); // get total & count from this array
    $total = (int)$result["data"]["total"]; // total rigs
    $count = (int)$result["data"]["count"]; // rigs output

    $result_arr = array();  // array for All search data

    /* if total amount > count */
    if ($total > $count ) {

        $offset = 0;
        $tot = $total;

        while($tot > 0){

            $search = array(
                "count"=> 100,
                "offset"=> $offset,
                "type"=>$algo,
                "rented"=>"true"
            );

            $result_item = $mrr->get("/rig",$search);
            $result_item = $result_item["data"]["records"];

            $result_arr = array_merge($result_arr, $result_item);

            $offset = $offset + 100; //increment offset
            $tot = $tot - 100; // decrement
        }

    } else {

        $result_arr = $result;
    }

	$result_array = array();
	$detailed_array = array();
//	foreach ($result as $key => $rig) {
	foreach ($result_arr as $key => $rig) {
		if ($rig["status"]["status"] == "rented") {
			$result_array[] = $rig["id"];
			# detailed array stores values for further writing to log
			# algo, name, owner, 
			$detailed_array[$rig["id"]] = array(
				"algo" => $algo,
				"id" => $rig["id"],
				"name" => $rig["name"],
				"owner" => $rig["owner"],
				"price" => $rig["price"]["BTC"]["price"],
				"advertised_hash" => $rig["hashrate"]["advertised"]["hash"],
				"hashrate_30" => $rig["hashrate"]["last_30min"]["hash"],
				"rentedhours" => $rig["status"]["hours"],
				"minhours" => $rig["minhours"],
			);
		}
		
	}

	sort($result_array);
	$result = array (
		"result_array" => $result_array,
		"detailed_array" => $detailed_array
	);
	return $result;

}

# goes through all algos in AlgosAndRentedRigs.txt and checks if some new rigs where rented
function checkAlgos() {
	global $algos_and_rented_rigs;

	foreach ($algos_and_rented_rigs as $algo_name => $value) {
		checkAlgo($algo_name);
	}
	
	clearOldLog();
}

# checks if algo was changed (new rigs added)
function checkAlgo($algo) {
	global $algos_and_rented_rigs;

	$getAlgoRented = getAlgoRented($algo);
	$rented_rig_ids = $getAlgoRented["result_array"];

	$rented_rig_ids_json = json_encode($rented_rig_ids);
	
	$algos_and_rented_rigs_json = json_encode($algos_and_rented_rigs[$algo]);


	if ($rented_rig_ids_json == $algos_and_rented_rigs_json) {
		echo "Nothing changed for ".$algo."<br>";
	} else {
		# if something changed from last time, check if new rig added
		# if yes:
		# write to log
		# update data in file

		echo "Changes for ".$algo."<br>";
		$save = 'false';

		foreach ($rented_rig_ids as $key => $rig_id) {
			# if rig not in array then act
			if (is_array($algos_and_rented_rigs[$algo]) AND (!in_array($rig_id, $algos_and_rented_rigs[$algo]))) {
				
				echo $rig_id." NOT in array<br>";

				$data_to_write = json_encode($getAlgoRented["detailed_array"][$rig_id]);

				# $save = true if writeToLog succedeed
				$save = writeToLog($data_to_write);
			}

		}

		# save new rented rig ids to file
		if ($save) {
			setAlgoAndRentedRigs($algo, $rented_rig_ids);
		}
		
	}

	

}

function writeToLog($data) {
	if (isset($data)) {
		date_default_timezone_set ( "Europe/Kiev" );
		$date = date("Y-m-d H:i:s");
		$data = mysql_escape_string($data);
		$query = "INSERT INTO `algos_rented_log` SET `data` = '$data', `date` = '$date'";
		base_query($query);
		return true;
	} else {
		return false;
	}
}

function clearOldLog() {
	date_default_timezone_set ( "Europe/Kiev" );
	$date = date("Y-m-d H:i:s", time()-60*60*24*5);
	$query = "DELETE FROM `algos_rented_log` WHERE `date` < '$date'";
	base_query($query);
}

function setAlgoAndRentedRigs($algo,$new_rig_ids) {
	$algos_and_rented_rigs_array = json_decode(file_get_contents("AlgosAndRentedRigs.txt"), true);
	$algos_and_rented_rigs_array[$algo] = $new_rig_ids;
	file_put_contents("AlgosAndRentedRigs.txt", json_encode($algos_and_rented_rigs_array));
}

# reads file and returns algo and rented rigs list
# for further monitoring and logging
function getAlgosAndRentedRigs() {
	$algo_list = json_decode(file_get_contents("AlgosAndRentedRigs.txt"), true);
	ksort($algo_list);
	return $algo_list;
}

function addAlgo($algo) {
	setAlgoAndRentedRigs($algo,"");
}

function deleteAlgo($algo) {
	$algos_and_rented_rigs_array = json_decode(file_get_contents("AlgosAndRentedRigs.txt"), true);
	$algos_and_rented_rigs_array_new = array();

	foreach ($algos_and_rented_rigs_array as $algo_name => $ids_list) {
		if (($algo_name != $algo) AND (!array_key_exists($algo, $algos_and_rented_rigs_array_new))) {
			$algos_and_rented_rigs_array_new[$algo_name] = $ids_list;
		}
	}
	
	file_put_contents("AlgosAndRentedRigs.txt", json_encode($algos_and_rented_rigs_array_new));
}


?>