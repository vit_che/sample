<?php

require_once __DIR__ . '/../libs/functions.php';
require_once __DIR__ . '/../mrr/mrr.php';

function setData($checked_id, $nochecked_id, $minprice){
    /* save checked id rigs */
    if (isset($checked_id)){
        file_put_contents(__DIR__."/../zombie/rig_list.txt", $checked_id);
    }
    /* save no checked id rigs */
    if (isset($nochecked_id)){
        file_put_contents(__DIR__."/../all_rigs/nochecked_rig_list.txt", $nochecked_id);
    }
    /* save min price rigs */
    if (isset($minprice)){
        file_put_contents(__DIR__."/../all_rigs/rig_min_prices.txt", $minprice);
    }

}

$checked_id = json_encode($_POST["idcheck"]);
$nochecked_id = json_encode($_POST["idnocheck"]);
$minprices = json_encode($_POST["minprice"]);


setData($checked_id, $nochecked_id, $minprices);

?>

